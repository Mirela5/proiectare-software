package controllers;


import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import domain.*;
import domain.questions.InvalidQuestionTypeException;
import domain.questions.Question;
import domain.questions.QuestionFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;

import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Controller pentru conexiunea la baza de date MySQL
 */
public class DatabaseController {

    /**
     * Spring singleton
     *
     * @return Connection - Conexiune noua
     * @throws ClassNotFoundException
     * @throws SQLException
     */
    @Bean
    @Scope("singleton")
    public static Connection getConnection() throws ClassNotFoundException, SQLException {
        Class.forName("com.mysql.cj.jdbc.Driver");

        return DriverManager.getConnection("jdbc:mysql://localhost:3306/feedbackapp", "root", "");
    }

    /**
     * Insereaza intrebarile unei completari de quiz
     *
     * @param q            Completarea pentru care se face inserarea intrebarilor
     * @param con          Conexiunea la baza de date
     * @param generalState statement care se executa
     * @param quizid       - id-ul quiz-ului completat
     * @param completionId - id-ul completarii
     * @return Raspunsul in format json ({"msg" : "success"} daca nu apar erori - {"msg" : "TEXT_EROARE"} daca sunt erori)
     * @throws SQLException
     */
    private static String insertQuestionsForCompletion(Completion q, Connection con, Statement generalState, Long quizid, Long completionId) throws SQLException {
        for (String question : q.responses.keySet()) {
            ResultSet questionIdRS = con.createStatement().executeQuery("SELECT questionId from questions WHERE quizid = " + quizid + " and text='" + question + "'");
            if (questionIdRS.next()) {
                long questionId = questionIdRS.getLong("questionId");
                con.createStatement().executeUpdate("INSERT INTO completion_question (questionId,completion_id) VALUES(" +
                        questionId + "," +
                        completionId +
                        ")");
                ResultSet completionQuestionIdRS = generalState.executeQuery("SELECT LAST_INSERT_ID()");
                if (completionQuestionIdRS.next()) {
                    Long completionQuestinId = completionQuestionIdRS.getLong(1);
                    insertAnswersForQuestion(q, con, question, completionQuestinId);
                }
            } else {
                return "{\"response\":\"There is no question with title '" + question + "'\"}";
            }
        }
        return null;
    }

    /**
     * Insereaza varinatele de raspuns pentru completarea quiz-ului
     *
     * @param q                   - Completarea pentru care se insereaza
     * @param con                 Conexiunea la baza de date
     * @param question            - Intrebarea pentru care se face inserarea;
     * @param completionQuestinId - id-ul intrebarii
     * @throws SQLException
     */
    private static void insertAnswersForQuestion(Completion q, Connection con, String question, Long completionQuestinId) throws SQLException {
        for (String answer : q.responses.get(question)) {
            ResultSet answervariantidRS = con.createStatement().executeQuery("SELECT answervariantid FROM answer_variants WHERE text = '" + answer + "'");
            if (answervariantidRS.next()) {
                long answerVariantid = answervariantidRS.getLong("answervariantid");
                con.createStatement().executeUpdate("INSERT INTO completion_answers (answer_variant_id,completion_question_id) VALUES (" +
                        answerVariantid + "," +
                        completionQuestinId + ")");
            } else {
                con.createStatement().executeUpdate("UPDATE completion_question SET freeAnswerResponse = '" + answer + "' WHERE completion_question_id = " + completionQuestinId);
            }
        }
    }

    /**
     * Insereaza datele unei completari de chestionar in baza de date
     *
     * @param q        obiectul de tip Completion
     * @param observer
     * @return Raspunsul in format json ({"msg" : "success"} daca nu apar erori - {"msg" : "TEXT_EROARE"} daca sunt erori)
     * @throws ClassNotFoundException
     * @throws SQLException
     */
    static String insertCompletion(Completion q, PointsObserver observer) throws ClassNotFoundException, SQLException {
        Connection con = DatabaseController.getConnection();
        Statement generalState = con.createStatement();
        ResultSet quizIdRS = generalState.executeQuery("SELECT quizid,reward FROM quizzes WHERE quizKey = '" + q.quizKey + "'");

        if (quizIdRS.next()) {
            long quizid = quizIdRS.getLong("quizid");
            long points = quizIdRS.getLong("reward");
            generalState.executeUpdate("INSERT INTO completion(userToken,timestamp,quizid) VALUES (" +
                    "'" + q.userToken + "'," +
                    "" + q.timestamp.getTime() / 1000 + "," +
                    "" + quizid + "" +
                    ")");
            quizIdRS = con.createStatement().executeQuery("SELECT LAST_INSERT_ID()");
            if (quizIdRS.next()) {
                Long completionId = quizIdRS.getLong(1);
                String question = insertQuestionsForCompletion(q, con, generalState, quizid, completionId);
                if (question != null) return question;
            }
            observer.onCompletionInserted(q.userToken, points);

        } else {
            return "{\"response\":\"There is no quiz with key '" + q.quizKey + "'\"}";
        }
        return null;
    }

    /**
     * Inserts quiz in the database
     *
     * @param quiz the quiz to be inserted
     * @return the response if any
     */
    static String insertQuiz(Quiz quiz) {
        try {


            Connection con = DatabaseController.getConnection();

            // insereaza quiz-ul
            Statement stmt = con.createStatement();
            String sql = String.format("INSERT INTO quizzes (title,quizkey,timestamp,reward) values ('%s','%s',%d, %d)", quiz.name, quiz.quizKey, quiz.timestamp.getTime() / 1000, quiz.reward);
            stmt.executeUpdate(sql);

            // aduce ultimul id inserat in baza de date
            ResultSet rs = getLastId(con, stmt);

            insertQuestionForQuiz(quiz, con, rs);

        } catch (ClassNotFoundException e) {
            return "{\"response\":\"There was an error connecting to the db\"}";
        } catch (SQLException e) {
            return String.format("{\"response\":\"There was an error in the sql: %s\"}", e.getMessage());
        }
        return "{\"response\":\"success\"}";
    }

    /**
     * Method to get the last inserted id
     *
     * @param con  Database Connection
     * @param stmt Statement to be run
     * @return Result set with the id
     * @throws SQLException
     */
    private static ResultSet getLastId(Connection con, Statement stmt) throws SQLException {
        String sql;
        sql = "SELECT LAST_INSERT_ID()";
        ResultSet rs = con.createStatement().executeQuery(sql);
        stmt.close();
        return rs;
    }

    /**
     * Insereaza in baza de date toate intrebarile unui quiz
     *
     * @param quiz quiz-ul pentru care se insereaza intrebarile
     * @param con  conexiunea la baza de date
     * @param rs   - result set-ul care contine id-ul quiz-ului inserat
     * @throws SQLException
     */
    private static void insertQuestionForQuiz(Quiz quiz, Connection con, ResultSet rs) throws SQLException {

        if (rs.next()) {
            long finalQuizid = rs.getLong(1);
            for (Question question : quiz.questions) {
                Statement insertStmt = con.createStatement();

                String insertSql = "INSERT INTO questions (text,questionType,multipleAnswer,quizId) VALUES ( ";
                insertSql += "'" + question.getQuestion() + "',";
                insertSql += "'" + question.getQuestionType() + "',";
                insertSql += (question.getMultipleAnswer() ? 1 : 0) + ",";
                insertSql += finalQuizid + ")";

                insertStmt.executeUpdate(insertSql);


                String getSql = "SELECT LAST_INSERT_ID()";
                ResultSet idRS = insertStmt.executeQuery(getSql);
                InsertAnswerVariantsForQuesttion(con, question, idRS);

            }
        }
    }

    /**
     * Insereaza in baza de date toate variantele de raspuns ale intrebarilor
     *
     * @param con      - conexiunea la baza de date
     * @param question - intrebarea pentru care se insereaza variantele de raspuns
     * @param idRS     - result set-ul care contine id-ul intrebarii inserate.
     * @throws SQLException
     */
    private static void InsertAnswerVariantsForQuesttion(Connection con, Question question, ResultSet idRS) throws SQLException {

        if (idRS.next()) {
            long questionId = idRS.getLong(1);
            for (String answerVariant : question.answerVariants) {
                Statement answerVariantsStmt = con.createStatement();
                String insertSQL = String.format("INSERT INTO answer_variants (text,questionid) VALUES ('%s','%s'", answerVariant, questionId);
                answerVariantsStmt.executeUpdate(insertSQL);
            }
        }
    }

    /**
     * Inserts User in the database
     *
     * @param u the User to be inserted
     * @return the response if any
     */
    static void insertUser(User u) throws SQLException, ClassNotFoundException {
        Statement stmt = DatabaseController.getConnection().createStatement();
        String insertAnswerVariantSql = String.format("INSERT INTO users (username,password,points) VALUES ('%s','%s',0)", u.username, u.password);
        stmt.executeUpdate(insertAnswerVariantSql);

    }

    /**
     * Validates user
     *
     * @param username - string
     * @param password - string
     * @return the access token or error message
     * @throws Exception
     */
    static String validateUser(String username, String password) throws Exception {
        Statement stmt = DatabaseController.getConnection().createStatement();
        String validateSQL = String.format("SELECT userid FROM users WHERE username='%s' AND password='%s'", username, password);
        ResultSet rs = stmt.executeQuery(validateSQL);

        if (rs.next()) {
            long uid = rs.getLong("userid");
            return MiscController.generateToken(uid);
        }
        throw new Exception("Invalid credentials provided");


    }

    /**
     * Adds points to the user
     *
     * @param token user's acces token
     * @param toAdd number of points to be added
     * @throws SQLException
     * @throws ClassNotFoundException
     */
    static void updatePointsForUser(String token, long toAdd) throws SQLException, ClassNotFoundException {

        long uid = MiscController.getUIDFromToken(token);
        String getSQL = String.format("SELECT points FROM USERs WHERE userid = %d", uid);
        ResultSet rs = getConnection().createStatement().executeQuery(getSQL);
        if (rs.next()) {
            long currentPoints = rs.getLong("points");
            String insertSql = String.format("UPDATE USERS SET points = %d WHERE userid = %d", currentPoints + toAdd, uid);
            getConnection().createStatement().executeUpdate(insertSql);
        }


    }

    /**
     * Gets a quiz based on key
     *
     * @param key - the key of a quiz ( from qr code)
     * @return the quizz or an error
     * @throws SQLException
     * @throws ClassNotFoundException
     * @throws InvalidQuestionTypeException
     * @throws JsonProcessingException
     */
    static String getQuizByKey(String key) throws SQLException, ClassNotFoundException, InvalidQuestionTypeException, JsonProcessingException {
        String getSQL = String.format("SELECT `title`,`quizKey`,`timestamp`,`reward`, quizid FROM `quizzes` WHERE quizKey = '%s'", key);

        ResultSet rs = getConnection().createStatement().executeQuery(getSQL);
        if (rs.next()) {
            Quiz q = new Quiz();
            q.reward = Math.toIntExact(rs.getLong("reward"));
            q.quizKey = rs.getString("quizKey");
            q.timestamp = new Date(rs.getLong("timestamp") * 1000);
            q.name = rs.getString("quizKey");
            q.questions = getQuestionsForQuiz(rs.getInt("quizid"));

            if (q.questions == null)
                return "There was an error";
            return new ObjectMapper().writeValueAsString(q);
        }
        return String.format("There is no quiz with this key='%s'", key);

    }


    /**
     * Gets answer variants for a given quiz
     *
     * @param quizid - quiz id
     * @return list of questions or error
     * @throws SQLException
     * @throws ClassNotFoundException
     * @throws InvalidQuestionTypeException
     * @throws JsonProcessingException
     */
    private static ArrayList<Question> getQuestionsForQuiz(int quizid) throws SQLException, ClassNotFoundException, InvalidQuestionTypeException {
        String getSQL = String.format("SELECT `questionid`,`text`,`questionType`,`multipleAnswer` FROM `questions` where quizid = %d", quizid);
        QuestionFactory factory = new QuestionFactory();
        ResultSet rs = getConnection().createStatement().executeQuery(getSQL);
        ArrayList<Question> q = null;
        while (rs.next()) {
            if (q == null)
                q = new ArrayList<>();
            String questionType = rs.getString("questionType");
            boolean multipleAnswer = rs.getBoolean("text");
            String text = rs.getString("questionType");
            long questionId = rs.getLong("questionid");
            Question question = factory.getQuestion(questionType, text, multipleAnswer, getAnswerVariantsForQuestion(questionId));
            if (question == null || !questionType.equalsIgnoreCase("free") && question.answerVariants == null)
                return null;
            q.add(question);

        }
        return q;
    }

    /**
     * Gets answer variants for a given quiz
     *
     * @param questionId - question id
     * @return list of string or error
     * @throws SQLException
     * @throws ClassNotFoundException
     * @throws InvalidQuestionTypeException
     * @throws JsonProcessingException
     */
    private static ArrayList<String> getAnswerVariantsForQuestion(long questionId) throws SQLException, ClassNotFoundException {
        String getSQL = String.format("SELECT `text` FROM `answer_variants` WHERE  where `questionid` = %d", questionId);

        ResultSet rs = getConnection().createStatement().executeQuery(getSQL);
        ArrayList<String> q = null;
        while (rs.next()) {
            if (q == null)
                q = new ArrayList<>();
            q.add(rs.getString("text"));

        }
        return q;
    }

    /**
     * Gets statistics for a given quiz
     *
     * @param key - quiz key
     * @return list of Statistic items or error
     * @throws SQLException
     * @throws ClassNotFoundException
     * @throws JsonProcessingException
     */
    static String getStatisticsForQuiz(String key) throws SQLException, ClassNotFoundException, JsonProcessingException {
        String getSQL = String.format(
                "SELECT questions.text                                  AS question, \n" +
                "       CASE \n" +
                "         WHEN answer_variants.text IS NULL THEN \n" +
                "         completion_question.freeanswerresponse \n" +
                "         ELSE answer_variants.text \n" +
                "       end                                             AS answer, \n" +
                "       Count(completion_answers.answer_variant_id) \n" +
                "       + Count(completion_question.freeanswerresponse) AS COUNT \n" +
                "FROM   quizzes \n" +
                "       LEFT JOIN questions \n" +
                "              ON questions.quizid = quizzes.quizid \n" +
                "       LEFT JOIN answer_variants \n" +
                "              ON questions.questionid = answer_variants.questionid \n" +
                "       LEFT JOIN completion_question \n" +
                "              ON questions.questionid = completion_question.questionid \n" +
                "       LEFT JOIN completion_answers \n" +
                "              ON answer_variants.answervariantid = \n" +
                "                 completion_answers.answer_variant_id \n" +
                "WHERE  quizzes.quizkey = '%s' \n" +
                "GROUP  BY questions.text, \n" +
                "          answer_variants.text, \n" +
                "          completion_question.freeanswerresponse ",
                key
        );

        ResultSet rs = getConnection().createStatement().executeQuery(getSQL);
        HashMap<String, List<StatisticItem>> map = new HashMap<>();
        boolean exists = false;
        while (rs.next()) {
            exists = true;
            String question = rs.getString("question");
            String answer = rs.getString("answer");
            long count = rs.getLong("count");
            if (map.containsKey(question)) {
                map.get(question).add(new StatisticItem(answer, count));
            } else {
                List<StatisticItem> items = new ArrayList<>();
                items.add(new StatisticItem(answer, count));
                map.put(question, items);
            }
        }
        if (!exists)
            return String.format("There is no quiz with this key='%s'", key);
        else {
            List<Statistic> stats = new ArrayList<>();
            for (String question : map.keySet()) {
                stats.add(new Statistic(question, map.get(question)));
            }
            return new ObjectMapper().writeValueAsString(stats);
        }


    }


    public static String insertToken(Long uid, String token) throws SQLException, ClassNotFoundException {
        String insertSQL = String.format("INSERT INTO tokens(userid,token) VALUES (%d,'%s')",uid, token );
        DatabaseController.getConnection().createStatement().executeUpdate(insertSQL);
        return String.format("{\"response\":\"%s\"}",token);
    }
}
