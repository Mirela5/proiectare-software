package controllers;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import domain.Completion;
import domain.PointsObserver;
import domain.Quiz;
import domain.User;
import domain.questions.InvalidQuestionTypeException;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

/**
 * Controller principal prin care se prelucreaza request-urile de tip REST (Representational State Transfer)
 */
@RestController
@EnableAutoConfiguration
public class MainController {


    private PointsObserver observer = DatabaseController::updatePointsForUser;

    public MainController(){

    }
    // Insert Endpoints

    /**
     * Insereaza obiectul de tip Quiz in baza de date.
     *
     * @param quizJson Quiz in format json
     * @return Raspunsul in format json ({"msg" : "success"} daca nu apar erori - {"msg" : "TEXT_EROARE"} daca sunt erori)
     */
    @RequestMapping(value = "/insertQuiz", method = RequestMethod.POST, produces = "application/json")
    public @ResponseBody String insertQuiz(@RequestParam String quizJson) {
        try {
            Quiz quiz = new ObjectMapper().readValue(quizJson, Quiz.class);
            return DatabaseController.insertQuiz(quiz);
        }catch (JsonParseException e) {
            return "{\"response\":\"There was an error parsing the json\"}";
        } catch (JsonMappingException e) {
            return "{\"response\":\"There was an error maping the json\"}";
        } catch (IOException e) {
            return "{\"response\":\"There was an IO error \"}";
        }
    }

    /**
     * Insereaza datele unei completari de chestionar in baza de date
     *
     * @param completionJson - obiectul de tip Completion in format json
     * @return Raspunsul in format json ({"msg" : "success"} daca nu apar erori - {"msg" : "TEXT_EROARE"} daca sunt erori)
     */
    @RequestMapping(value = "/insertCompletion", method = RequestMethod.POST, produces = "application/json")
    public @ResponseBody String insertCompletion(@RequestParam String completionJson) {
        Completion c;
        try {
            c = new ObjectMapper().readValue(completionJson, Completion.class);
            String response = DatabaseController.insertCompletion(c,observer);
            if (response != null) return response;
            return "{\"response\":\"success\"}";

        } catch (IOException | ClassNotFoundException | SQLException e) {
            return "{\"response\":\"" + e.getMessage() + "\"}";
        }

    }

    /**
     * Insereaza un user in baza de date
     *
     * @param userJson - obiectul de tip User in format json
     * @return Raspunsul in format json ({"msg" : "success"} daca nu apar erori - {"msg" : "TEXT_EROARE"} daca sunt erori)
     */
    @RequestMapping(value = "/insertUser", method = RequestMethod.POST, produces = "application/json")
    public @ResponseBody String insertUser(@RequestParam String userJson) {
        try {
            User u = new ObjectMapper().readValue(userJson, User.class);
            DatabaseController.insertUser(u);
            return String.format("{\"response\":\"%s\"}", "success");
        } catch (Exception e) {
            return String.format("{\"response\":\"%s\"}", e.getMessage());
        }

    }

    // Get Endpoints

    /**
     * Gets a quiz based on key
     *
     * @param key - the key of a quiz ( from qr code)
     * @return the quizz or an error
     * @throws SQLException
     * @throws ClassNotFoundException
     * @throws InvalidQuestionTypeException
     * @throws JsonProcessingException
     */
    @RequestMapping(value = "/getQuizByKey", method = RequestMethod.POST, produces = "application/json")
    public @ResponseBody String getQuizByKey(@RequestParam String key) {
        try {
            return String.format("{\"response\":\"%s\"}", DatabaseController.getQuizByKey(key));
        } catch (Exception | InvalidQuestionTypeException e) {
            return String.format("{\"response\":\"%s\"}", e.getMessage());
        }
    }

    /**
     * Gets statistics for a given quiz
     *
     * @param key - quiz key
     * @return list of Statistic items or error
     * @throws SQLException
     * @throws ClassNotFoundException
     * @throws JsonProcessingException
     */
    @RequestMapping(value = "/getStatistcsForQuiz", method = RequestMethod.POST, produces = "application/json")
    public @ResponseBody String getStatistcsForQuiz(@RequestParam String key) {
        try {
            return String.format("{\"response\":\"%s\"}", DatabaseController.getStatisticsForQuiz(key));
        } catch (Exception e) {
            return String.format("{\"response\":\"%s\"}", e.getMessage());
        }
    }

    @RequestMapping(value = "/getCompletion", method = RequestMethod.POST, produces = "application/json")
    public @ResponseBody Completion getCompletion() {
        Completion c = new Completion();
        c.quizKey = "54c03f7f-43ed-43e4-88b8-250fd02d1e8f";
        c.userToken = "MXwyMDE5LTA0LTE3IDA2OjIxOjA0";
        c.timestamp = new Date();
        c.responses = new HashMap<>();
        c.responses.put("Is it gonna work?", new ArrayList<>());
        c.responses.get("Is it gonna work?").add("It is not gonna work");
        c.responses.get("Is it gonna work?").add("It is gonna work");
        return c;

    }

    // Generation and Validation Endpoints

    /**
     * Genereaza cheia unui quiz pentru generarea unui cod QR
     *
     * @return Raspunsul in format json ({"msg" : "success"} daca nu apar erori - {"msg" : "TEXT_EROARE"} daca sunt erori)
     */
    @RequestMapping(value = "/generateQuizKey", method = RequestMethod.POST, produces = "application/json")
    public @ResponseBody String generateQuizKey() {

        try {

            String sb = MiscController.getSHA256String();
            return "{\"quizKey\":\"" + sb + "\"}";

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return "{\"response\":\"error\"}";
    }

    /**
     * Genereaza un token de acces pentru un utilizator (in cazul in care utilizatorul este valid
     *
     * @return Raspunsul in format json ({"msg" : "success"} daca nu apar erori - {"msg" : "TEXT_EROARE"} daca sunt erori)
     */
    @RequestMapping(value = "/validateUser", method = RequestMethod.POST, produces = "application/json")
    public @ResponseBody String validateUser(@RequestParam String username, @RequestParam String password) {
        try {
            return String.format("{\"response\":\"%s\"}", DatabaseController.validateUser(username,password));
        } catch (Exception e) {
            return String.format("{\"response\":\"%s\"}", e.getMessage());
        }
    }







}

