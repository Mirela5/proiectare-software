package domain;

public class StatisticItem {
    public String answer;
    public long count;

    public StatisticItem(String answer, long count) {
        this.answer = answer;
        this.count = count;
    }
}
