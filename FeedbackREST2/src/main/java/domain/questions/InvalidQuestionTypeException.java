package domain.questions;

public class InvalidQuestionTypeException extends Throwable {
    InvalidQuestionTypeException(String msg) {
        super(msg);
    }
}
