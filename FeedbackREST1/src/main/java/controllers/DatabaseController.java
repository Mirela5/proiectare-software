package controllers;


import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * Controller pentru conexiunea la baza de date MySQL
 */
public class DatabaseController {

    /**
     * Spring singleton
     *
     * @return Connection - Conexiune noua
     * @throws ClassNotFoundException
     * @throws SQLException
     */
    @Bean
    @Scope("singleton")
    public static Connection getConnection() throws ClassNotFoundException, SQLException {
        Class.forName("com.mysql.cj.jdbc.Driver");

        return DriverManager.getConnection("jdbc:mysql://localhost:3306/feedbackapp", "root", "");
    }

}
