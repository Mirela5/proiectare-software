package controllers;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import domain.Completion;
import domain.Quiz;
import domain.questions.Question;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.UUID;

/**
 * Controller principal prin care se prelucreaza request-urile de tip REST (Representational State Transfer)
 */
@RestController
@EnableAutoConfiguration
public class MainController {
    /**
     * Insereaza obiectul de tip Quiz in baza de date.
     *
     * @param quizJson Quiz in format json
     * @return Raspunsul in format json ({"msg" : "success"} daca nu apar erori - {"msg" : "TEXT_EROARE"} daca sunt erori)
     */
    @RequestMapping(value = "/insertQuiz", method = RequestMethod.POST, produces = "application/json")
    public @ResponseBody
    String insertQuiz(@RequestParam String quizJson) {
        try {
            Quiz quiz = new ObjectMapper().readValue(quizJson, Quiz.class);

            Connection con = DatabaseController.getConnection();

            // insereaza quiz-ul
            Statement stmt = con.createStatement();
            String sql = "INSERT INTO quizzes (title,quizkey,timestamp) values ('" + quiz.name + "','" + quiz.quizKey + "'," + quiz.timestamp.getTime() / 1000 + ")";
            stmt.executeUpdate(sql);

            // aduce ultimul id inserat in baza de date
            sql = "SELECT LAST_INSERT_ID()";
            ResultSet rs = con.createStatement().executeQuery(sql);
            stmt.close();

            insertQuestionForQuiz(quiz, con, rs);

        } catch (ClassNotFoundException e) {
            return "{\"msg\":\"There was an error connecting to the db\"}";
        } catch (SQLException e) {
            return "{\"msg\":\"There was an error in the sql: " + e.getMessage() + "\"}";
        } catch (JsonParseException e) {
            return "{\"msg\":\"There was an error parsing the json\"}";
        } catch (JsonMappingException e) {
            return "{\"msg\":\"There was an error maping the json\"}";
        } catch (IOException e) {
            return "{\"msg\":\"There was an IO error \"}";
        }
        return "{\"msg\":\"success\"}";
    }

    /**
     * Insereaza in baza de date toate intrebarile unui quiz
     *
     * @param quiz quiz-ul pentru care se insereaza intrebarile
     * @param con  conexiunea la baza de date
     * @param rs   - result set-ul care contine id-ul quiz-ului inserat
     * @throws SQLException
     */
    private void insertQuestionForQuiz(Quiz quiz, Connection con, ResultSet rs) throws SQLException {

        if (rs.next()) {
            long finalQuizid = rs.getLong(1);
            for (Question question : quiz.questions) {
                Statement insertStmt = con.createStatement();

                String insertSql = "INSERT INTO questions (text,questionType,multipleAnswer,quizId) VALUES ( ";
                insertSql += "'" + question.getQuestion() + "',";
                insertSql += "'" + question.getQuestionType() + "',";
                insertSql += (question.getMultipleAnswer() ? 1 : 0) + ",";
                insertSql += finalQuizid + ")";

                insertStmt.executeUpdate(insertSql);


                String getSql = "SELECT LAST_INSERT_ID()";
                ResultSet idRS = insertStmt.executeQuery(getSql);
                InsertAnswerVariantsForQuesttion(con, question, idRS);

            }
        }
    }

    /**
     * Insereaza in baza de date toate variantele de raspuns ale intrebarilor
     *
     * @param con      - conexiunea la baza de date
     * @param question - intrebarea pentru care se insereaza variantele de raspuns
     * @param idRS     - result set-ul care contine id-ul intrebarii inserate.
     * @throws SQLException
     */
    private void InsertAnswerVariantsForQuesttion(Connection con, Question question, ResultSet idRS) throws SQLException {

        if (idRS.next()) {
            long questionId = idRS.getLong(1);
            for (String answerVariant : question.answerVariants) {
                Statement answerVariantsStmt = con.createStatement();
                String insertAnswerVariantSql = "INSERT INTO answer_variants (text,questionid) VALUES ( ";
                insertAnswerVariantSql += "'" + answerVariant + "',";
                insertAnswerVariantSql += questionId + ")";
                answerVariantsStmt.executeUpdate(insertAnswerVariantSql);
            }
        }
    }

    /**
     * Genereaza cheia unui quiz pentru generarea unui cod QR
     *
     * @return Raspunsul in format json ({"msg" : "success"} daca nu apar erori - {"msg" : "TEXT_EROARE"} daca sunt erori)
     */
    @RequestMapping(value = "/generateQuizKey", method = RequestMethod.POST, produces = "application/json")
    public @ResponseBody
    String generateQuizKey() {
        MessageDigest salt;
        try {

            salt = MessageDigest.getInstance("SHA-256");
            // geneam un byte array sha-256 (256 biti) pe baza unui guid ranom (unic)
            salt.update(UUID.randomUUID().toString().getBytes(StandardCharsets.UTF_8));
            StringBuilder sb = new StringBuilder();
            // dupa care transformam byte array-ul intr-un string cu format hexa cu 2 cifre
            for (byte b : salt.digest()) {
                sb.append(String.format("%02x", b));
            }
            return "{\"quizKey\":\"" + sb.toString() + "\"}";

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return "{\"msg\":\"error\"}";
    }

    /**
     * Insereaza datele unei completari de chestionar in baza de date
     *
     * @param completionJson - obiectul de tip Completion in format json
     * @return Raspunsul in format json ({"msg" : "success"} daca nu apar erori - {"msg" : "TEXT_EROARE"} daca sunt erori)
     */
    @RequestMapping(value = "/insertCompletion", method = RequestMethod.POST, produces = "application/json")
    public @ResponseBody
    String insertCompletion(@RequestParam String completionJson) {
        Completion q;
        try {
            q = new ObjectMapper().readValue(completionJson, Completion.class);
            String question = insertCompletion(q);
            if (question != null) return question;
            return "{\"msg\":\"success\"}";

        } catch (IOException | ClassNotFoundException | SQLException e) {
            return "{\"msg\":\"" + e.getMessage() + "\"}";
        }

    }

    /**
     * Insereaza datele unei completari de chestionar in baza de date
     *
     * @param q obiectul de tip Completion
     * @return Raspunsul in format json ({"msg" : "success"} daca nu apar erori - {"msg" : "TEXT_EROARE"} daca sunt erori)
     * @throws ClassNotFoundException
     * @throws SQLException
     */
    private String insertCompletion(Completion q) throws ClassNotFoundException, SQLException {
        Connection con = DatabaseController.getConnection();
        Statement generalState = con.createStatement();
        ResultSet quizIdRS = generalState.executeQuery("SELECT quizid FROM quizzes WHERE title = '" + q.quizTitle + "'");
        if (quizIdRS.next()) {
            long quizid = quizIdRS.getLong("quizid");
            generalState.executeUpdate("INSERT INTO completion(userToken,timestamp,quizid) VALUES (" +
                    "'" + q.userToken + "'," +
                    "" + q.timestamp.getTime() / 1000 + "," +
                    "" + quizid + "" +
                    ")");
            quizIdRS = con.createStatement().executeQuery("SELECT LAST_INSERT_ID()");
            if (quizIdRS.next()) {
                Long completionId = quizIdRS.getLong(1);
                String question = insertQuestionsForCompletion(q, con, generalState, quizid, completionId);
                if (question != null) return question;
            }

        } else {
            return "{\"msg\":\"There is no quiz with title '" + q.quizTitle + "'\"}";
        }
        return null;
    }

    /**
     * Insereaza intrebarile unei completari de quiz
     *
     * @param q            Completarea pentru care se face inserarea intrebarilor
     * @param con          Conexiunea la baza de date
     * @param generalState statement care se executa
     * @param quizid       - id-ul quiz-ului completat
     * @param completionId - id-ul completarii
     * @return Raspunsul in format json ({"msg" : "success"} daca nu apar erori - {"msg" : "TEXT_EROARE"} daca sunt erori)
     * @throws SQLException
     */
    private String insertQuestionsForCompletion(Completion q, Connection con, Statement generalState, Long quizid, Long completionId) throws SQLException {
        for (String question : q.responses.keySet()) {
            ResultSet questionIdRS = con.createStatement().executeQuery("SELECT questionId from questions WHERE quizid = " + quizid + " and text='" + question + "'");
            if (questionIdRS.next()) {
                long questionId = questionIdRS.getLong("questionId");
                con.createStatement().executeUpdate("INSERT INTO completion_question (questionId,completion_id) VALUES(" +
                        questionId + "," +
                        completionId +
                        ")");
                ResultSet completionQuestionIdRS = generalState.executeQuery("SELECT LAST_INSERT_ID()");
                if (completionQuestionIdRS.next()) {
                    Long completionQuestinId = completionQuestionIdRS.getLong(1);
                    insertAnswersForQuestion(q, con, question, completionQuestinId);
                }
            } else {
                return "{\"msg\":\"There is no question with title '" + question + "'\"}";
            }
        }
        return null;
    }

    /**
     * Insereaza varinatele de raspuns pentru completarea quiz-ului
     *
     * @param q                   - Completarea pentru care se insereaza
     * @param con                 Conexiunea la baza de date
     * @param question            - Intrebarea pentru care se face inserarea;
     * @param completionQuestinId - id-ul intrebarii
     * @throws SQLException
     */
    private void insertAnswersForQuestion(Completion q, Connection con, String question, Long completionQuestinId) throws SQLException {
        for (String answer : q.responses.get(question)) {
            ResultSet answervariantidRS = con.createStatement().executeQuery("SELECT answervariantid FROM answer_variants WHERE text = '" + answer + "'");
            if (answervariantidRS.next()) {
                long answerVariantid = answervariantidRS.getLong("answervariantid");
                con.createStatement().executeUpdate("INSERT INTO completion_answers (answer_variant_id,completion_question_id) VALUES (" +
                        answerVariantid + "," +
                        completionQuestinId + ")");
            } else {
                con.createStatement().executeUpdate("UPDATE completion_question SET freeAnswerResponse = '" + answer + "' WHERE completion_question_id = " + completionQuestinId);
            }
        }
    }

}

