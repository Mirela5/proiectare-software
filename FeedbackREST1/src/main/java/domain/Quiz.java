package domain;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import domain.questions.Question;

import java.util.ArrayList;
import java.util.Date;

public class Quiz {
    public String quizKey;
    public String name;
    public ArrayList<Question> questions;
    public Date timestamp;

    public Quiz() {
    }

    @JsonCreator
    public Quiz(@JsonProperty("quizKey") String quizKey, @JsonProperty("name") String name, @JsonProperty("questions") ArrayList<Question> questions, @JsonProperty("timestamp") Date timestamp) {
        this.quizKey = quizKey;
        this.name = name;
        this.questions = questions;
        this.timestamp = timestamp;
    }
}
