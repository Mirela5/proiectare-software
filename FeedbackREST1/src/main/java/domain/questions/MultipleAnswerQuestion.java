package domain.questions;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.List;

public class MultipleAnswerQuestion extends Question {

    @JsonCreator
    public MultipleAnswerQuestion(@JsonProperty("question") String question, @JsonProperty("multipleAnswer") boolean multipleAnswer, @JsonProperty("answerVariants") ArrayList<String> answerVariants) {
        this.setQuestion(question);

        this.multipleAnswer = multipleAnswer;
        this.answerVariants = answerVariants != null ? answerVariants : new ArrayList<>();
    }

    @Override
    public String getQuestion() {
        return question;
    }

    @Override
    public void setQuestion(String question) {
        this.question = question;
    }

    @Override
    public List<String> getAnswerVariants() {
        return answerVariants;
    }

    @Override
    public void addAnswerVariant(String answerVariant) {
        this.answerVariants.add(answerVariant);
    }

    @Override
    public void removeAnswerVariant(String answerVariant) {
        this.answerVariants.remove(answerVariant);
    }

    @Override
    public void updateAnswerVariant(String oldAnswerVariant, String newAnswerVariant) {
        this.answerVariants.add(answerVariants.indexOf(oldAnswerVariant), newAnswerVariant);
        this.answerVariants.remove(oldAnswerVariant);
    }

    @Override
    public String getQuestionType() {
        return "multiple";
    }

    @Override
    public boolean getMultipleAnswer() {
        return multipleAnswer;
    }
}
