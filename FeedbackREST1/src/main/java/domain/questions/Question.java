package domain.questions;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonSubTypes.Type;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonTypeInfo.Id;

import java.util.ArrayList;
import java.util.List;

import static com.fasterxml.jackson.annotation.JsonTypeInfo.As.PROPERTY;

@JsonIgnoreProperties({"questionType"})
@JsonTypeInfo(use = Id.NAME,
        include = PROPERTY,
        property = "type")
@JsonSubTypes({
        @Type(value = FreeAnswerQuestion.class),
        @Type(value = MultipleAnswerQuestion.class),
})
public abstract class Question {

    public String question;
    public ArrayList<String> answerVariants;
    public boolean multipleAnswer;

    public Question() {

    }

    public abstract String getQuestion();

    public abstract void setQuestion(String question);

    public abstract List<String> getAnswerVariants();

    public abstract void addAnswerVariant(String answerVariant);

    public abstract void removeAnswerVariant(String answerVariant);

    public abstract void updateAnswerVariant(String oldAnswerVariant, String newAnswerVariant);

    public abstract String getQuestionType();

    public abstract boolean getMultipleAnswer();
}
