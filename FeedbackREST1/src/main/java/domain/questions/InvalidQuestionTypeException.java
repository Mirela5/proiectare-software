package domain.questions;

public class InvalidQuestionTypeException extends Throwable {
    public InvalidQuestionTypeException(String msg) {
        super(msg);
    }
}
