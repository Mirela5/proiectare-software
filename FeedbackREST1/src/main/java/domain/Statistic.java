package domain;

import java.util.ArrayList;
import java.util.List;

public class Statistic {
    public String question;
    public List<StatisticItem> answers;

    public Statistic(){
        answers = new ArrayList<>();

    }


    public Statistic(String question, List<StatisticItem> answers) {
        this.question = question;
        this.answers = answers;
    }
}
